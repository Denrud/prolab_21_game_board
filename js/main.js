//model
let ball = {status: false};
let genStatus = {grid: false};
let gridOption = [];

// variables
const form = document.querySelector('form');
const controls = document.querySelector('.controls');
const allControlBtn = document.querySelectorAll(".controls>span");
const btnGenerator = document.querySelector('button');
const redBtn = document.querySelector('.red-btn');
const redBallView = '<span class="red-ball"></span>';

const data = () => {
    const formData = new FormData(form);
    let col = Number(formData.get("col"));
    let row = Number(formData.get("row"));
    if ( col > 50 || row > 25 && col < 0 || row < 0 ) {
        return;
    }
    else {
        gridOption = [col, row];
        return  gridOption;
    }
}

const addCol = () =>{
    const col = gridOption[0];
    const findContainer = document.querySelector('.container');
    const createRow = document.createElement("div");
    createRow.classList.add('row');
    findContainer.appendChild(createRow);
    let genCol = "<span></span>";
    for (let gen = 0; gen < col; gen++) {  
        genCol;        
        createRow.insertAdjacentHTML("beforeend", genCol);  
    }
}

const addRow = () => {
    let row = gridOption[1];
    for (let gen = 0; gen < row - 1; gen++) {
        addCol();
    }
}

btnGenerator.addEventListener('click', () => {
    data();
    if ( gridOption.length === 0 || gridOption[0] === 0 || gridOption[1] === 0 ) {
        return;
    }
    else {
        console.log(gridOption)
        addCol();
        addRow();
        btnGenerator.classList.add('hidden');
        controls.classList.remove('hidden');
        redBtn.classList.remove('hidden');
        genStatus.grid = true;
        redBall();
    }
})


const redBall = () => {
    if ( !genStatus.grid ) {
        return;
    }
    if ( genStatus.grid ) {
        ball = {
            status: true,
            position: {x: 0, y: 0}
        }
        let x = document.querySelectorAll('.row')[0];
        let startPoint = x.querySelectorAll('span')[0]; 
        startPoint.insertAdjacentHTML('beforeend', redBallView); 
    }
}

const ballRowMove = ( rowDirect ) => {
    if ( rowDirect === 'ArrowDown' && ball.position.y !== gridOption[1] - 1 ) {
        document.querySelector('.red-ball').remove();
        ball.position.y += 1;
        console.log(ball.position.y);
        let x = document.querySelectorAll('.row')[ball.position.y];
        let startPoint = x.querySelectorAll('span')[ball.position.x];
        startPoint.insertAdjacentHTML('beforeend', redBallView);
        
    }
    else if ( rowDirect === 'ArrowUp' && ball.position.y !== 0 ) {
        document.querySelector('.red-ball').remove();
        ball.position.y -= 1;
        console.log(ball.position.y);
        let x = document.querySelectorAll('.row')[ball.position.y];
        let startPoint = x.querySelectorAll('span')[ball.position.x];
        startPoint.insertAdjacentHTML('beforeend', redBallView);
    }
    return ball.position.y;
}

const ballColMove = ( colIndex ) => {
    if ( colIndex === 'ArrowRight' && ball.position.x !== gridOption[0] - 1 ) {
        document.querySelector('.red-ball').remove();
        ball.position.x += 1;
        console.log(ball.position.x);
        let x = document.querySelectorAll('.row')[ball.position.y];
        let startPoint = x.querySelectorAll('span')[ball.position.x];
        startPoint.insertAdjacentHTML('beforeend', redBallView);
        
    }
    else if ( colIndex === 'ArrowLeft' && ball.position.x !== 0 ) {
        document.querySelector('.red-ball').remove();
        ball.position.x -= 1;
        console.log(ball.position.y);
        let x = document.querySelectorAll('.row')[ball.position.y];
        let startPoint = x.querySelectorAll('span')[ball.position.x];
        startPoint.insertAdjacentHTML('beforeend', redBallView);
    }
    return ball.position.x;     
}

window.addEventListener('keydown', (ev) => {
    console.log(ev.code);
    if (ev.code === 'ArrowLeft') {
        allControlBtn[0].classList.add('press-action');     
        ballColMove('ArrowLeft');
    }
    else if (ev.code === 'ArrowUp') {
        allControlBtn[1].classList.add('press-action');
        ballRowMove('ArrowUp');
    }
    else if (ev.code === 'ArrowDown') {
        allControlBtn[2].classList.add('press-action');
        ballRowMove('ArrowDown');
    }
    else if (ev.code === 'ArrowRight') {
        allControlBtn[3].classList.add('press-action');
        ballColMove('ArrowRight');
    }
})

window.addEventListener('keyup', (ev) => {
    if (ev.code === "ArrowLeft") {
        allControlBtn[0].classList.remove('press-action');
    }
    else if (ev.code === 'ArrowUp') {
        allControlBtn[1].classList.remove('press-action');
    }
    else if (ev.code === 'ArrowDown') {
        allControlBtn[2].classList.remove('press-action');
    }
    else if (ev.code === 'ArrowRight') {
        allControlBtn[3].classList.remove('press-action');
    }
})

